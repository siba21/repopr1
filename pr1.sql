-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Ned 02. čen 2019, 20:18
-- Verze serveru: 10.1.33-MariaDB
-- Verze PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `pr1`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `relace`
--

CREATE TABLE `relace` (
  `id_ucitele` int(11) NOT NULL,
  `id_titulu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Vypisuji data pro tabulku `relace`
--

INSERT INTO `relace` (`id_ucitele`, `id_titulu`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Struktura tabulky `tituly`
--

CREATE TABLE `tituly` (
  `id_titulu` int(11) NOT NULL,
  `titul` varchar(10) COLLATE utf8mb4_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Vypisuji data pro tabulku `tituly`
--

INSERT INTO `tituly` (`id_titulu`, `titul`) VALUES
(1, 'Mgr.'),
(2, 'Bc.');

-- --------------------------------------------------------

--
-- Struktura tabulky `ucitel`
--

CREATE TABLE `ucitel` (
  `id_ucitele` int(11) NOT NULL,
  `jmeno` varchar(20) COLLATE utf8mb4_czech_ci NOT NULL,
  `prijmeni` varchar(20) COLLATE utf8mb4_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Vypisuji data pro tabulku `ucitel`
--

INSERT INTO `ucitel` (`id_ucitele`, `jmeno`, `prijmeni`) VALUES
(1, 'Jan', 'Skromný'),
(2, 'Petrs', 'Novotný'),
(5, 'Holans', 'Testovičs');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `relace`
--
ALTER TABLE `relace`
  ADD PRIMARY KEY (`id_ucitele`,`id_titulu`),
  ADD KEY `fk_ucitel_has_tituly_tituly1_idx` (`id_titulu`),
  ADD KEY `fk_ucitel_has_tituly_ucitel_idx` (`id_ucitele`);

--
-- Klíče pro tabulku `tituly`
--
ALTER TABLE `tituly`
  ADD PRIMARY KEY (`id_titulu`);

--
-- Klíče pro tabulku `ucitel`
--
ALTER TABLE `ucitel`
  ADD PRIMARY KEY (`id_ucitele`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `tituly`
--
ALTER TABLE `tituly`
  MODIFY `id_titulu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pro tabulku `ucitel`
--
ALTER TABLE `ucitel`
  MODIFY `id_ucitele` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `relace`
--
ALTER TABLE `relace`
  ADD CONSTRAINT `fk_ucitel_has_tituly_tituly1` FOREIGN KEY (`id_titulu`) REFERENCES `tituly` (`id_titulu`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ucitel_has_tituly_ucitel` FOREIGN KEY (`id_ucitele`) REFERENCES `ucitel` (`id_ucitele`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
