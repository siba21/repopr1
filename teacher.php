<?php
    require_once("connect.php"); //nacteme si soubor kde mame PDOconnect funkci
    
    if($_GET) { //zjistim jestlim mam nejake promene v URL
      if(isset($_GET['del'])) { //zjistim jestli je v URL nastavena promena del
          if(isset($_GET['uid']) && isset($_GET['tid'])) { //zjistim jestli je v URL nastavena promena uid a tid  
            odeberTitul($_GET['uid'], $_GET['tid']);  // predam hodnoty uid a tid nastavene v URL funkci odeberTitul, ktera vykona smazani titulu 
            header("Location: seznam.php"); //presmeruju na seznam ucitelu
            exit; // zajisti ze se nevykona nasledujici kod po presmerovani      
          }
      } elseif(isset($_GET['edit'])){
        if(isset($_GET['uid'])) { //zjistim jestli je v URL nastavena promena uid
            $ucitel = vratUcitele($_GET['uid']);
            require_once("edit.php"); //nacteme si soubor kde mame edit ucitele   
        } else {
            header("Location: seznam.php"); //presmeruju na seznam ucitelu
            exit; // zajisti ze se nevykona nasledujici kod po presmerovani
        } 
      } elseif(isset($_GET['update'])) {
        if (isset($_POST["submit_edit_ucitele"])) {
            if(isset($_POST["jmeno"]) && isset($_POST["prijmeni"]) && isset($_POST["uid"])) {
                editujUcitele($_POST["uid"], $_POST["jmeno"], $_POST["prijmeni"]);
                header("Location: seznam.php"); //presmeruju na seznam ucitelu
                exit; // zajisti ze se nevykona nasledujici kod po presmerovani    
            }
        }
        
        header("Location: seznam.php"); //presmeruju na seznam ucitelu
        exit; // zajisti ze se nevykona nasledujici kod po presmerovani 
      }
    }
    
    function vypisSeznam() {
        $conn = PDOconnect("pr1", "root", ""); //vytvorime spojeni s DB se zadanymy udaji 
    
        try {
            $sql = "SELECT u.id_ucitele, t.id_titulu, u.jmeno, u.prijmeni, t.titul FROM ucitel u LEFT JOIN relace r ON u.id_ucitele = r.id_ucitele LEFT JOIN tituly t ON r.id_titulu = t.id_titulu; "; //sql dotaz na který se dotazuji
            $query = $conn->prepare($sql); //připravím si sql pro vykonání
            $query->execute(); // vykonám připravený sql dotaz
            
            return $query->fetchAll(PDO::FETCH_OBJ); //vrátí všechny získené řádky z dotazu jako pole objektů         
        } catch (PDOException $e) {
            echo "Vypis seznam selhal: " . $e->getMessage();
        }
    }
    
    function odeberTitul($uid, $tid) {
        $conn = PDOconnect("pr1", "root", ""); //vytvorime spojeni s DB se zadanymy udaji 
    
        try {
            $sql = "DELETE FROM relace WHERE id_ucitele = :idu AND id_titulu = :idt"; //sql dotaz na který se dotazuji
            $query = $conn->prepare($sql); //připravím si sql pro vykonání
            $query->bindParam(':idu', $uid); // vlozim parametr do dotazu
            $query->bindParam(':idt', $tid); // vlozim parametr do dotazu
            
            $query->execute(); // vykonám připravený sql dotaz         
        } catch (PDOException $e) {
            echo "odeberTitul selhal: " . $e->getMessage();
        }
    }
    
    function editujUcitele($uid, $jmeno, $prijmeni) {
        $conn = PDOconnect("pr1", "root", ""); //vytvorime spojeni s DB se zadanymy udaji
        
         try {
            $sql = "UPDATE ucitel SET jmeno = :jmeno, prijmeni = :prijmeni WHERE id_ucitele = :idu"; //sql dotaz na který se dotazuji
            $query = $conn->prepare($sql); //připravím si sql pro vykonání
            $query->bindParam(':idu', $uid); // vlozim parametr do dotazu
            $query->bindParam(':jmeno', $jmeno); // vlozim parametr do dotazu
            $query->bindParam(':prijmeni', $prijmeni); // vlozim parametr do dotazu
            
            $query->execute(); // vykonám připravený sql dotaz         
        } catch (PDOException $e) {
            echo "editujUcitele selhal: " . $e->getMessage();
        }    
    }
    
    function vratUcitele($uid) {
        $conn = PDOconnect("pr1", "root", ""); //vytvorime spojeni s DB se zadanymy udaji 
    
        try {
            $sql = "SELECT * FROM ucitel WHERE id_ucitele = :idu"; //sql dotaz na který se dotazuji
            $query = $conn->prepare($sql); //připravím si sql pro vykonání
            $query->bindParam(':idu', $uid); // vlozim parametr do dotazu
            
            $query->execute(); // vykonám připravený sql dotaz
            
            return $query->fetch(PDO::FETCH_OBJ); //vrati jeden radek s nalezenym ucitelem jako objekt         
        } catch (PDOException $e) {
            echo "vratUcitele selhal: " . $e->getMessage();
        }
    }
?>