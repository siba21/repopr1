<?php
    require_once("teacher.php"); // nactu teacher.php abych mohl pouzit funkci vypisSeznam
    $teachers = vypisSeznam(); //ziskam seznam ucitelu
?>

<!DOCTYPE html>
<html lang='cs'>
  <head>
    <title>Seznam učitelů</title>
    <meta charset='utf-8'>
  </head>
  <body>
    <table>
        <thead style="background-color: #ddd; font-weight: bold;">
        <tr>
            <td>Jméno</td>
            <td>Příjmení</td>
            <td>Tituly</td>
            <td>Editace</td>
            <td>Smazat titul</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($teachers as $teacher) { //cyklem projdu vsechny ucitele v poli a vypisu jejich atributy (jmeno, prijmeni, titul) ?>
            <tr>
                <td><?php if (isset($teacher->jmeno)) echo $teacher->jmeno;  //zjistim jestli je nastavene jmeno a kdyz ano tak ho vypisi ?></td>
                <td><?php if (isset($teacher->prijmeni)) echo $teacher->prijmeni; ?></td>
                <td><?php if (isset($teacher->titul)) echo $teacher->titul; ?></td>
                <td><a href="<?php if (isset($teacher->id_ucitele)) echo 'teacher.php?edit&uid=' . $teacher->id_ucitele; ?>">edit</a></td>
                <td><a href="<?php if (isset($teacher->id_ucitele, $teacher->id_titulu)) echo 'teacher.php?del&uid=' . $teacher->id_ucitele . '&tid=' . $teacher->id_titulu; ?>">smazat titul</a></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
  </body>
</html>