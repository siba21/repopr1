<?php
//funkce která po svém zavolání se připojí k databázi (buď pomocí zadaných parametrů nebo těch defaultně uvedených tady) a vrátí připojení k DB aby jsme s ním mohli pracovat
function PDOconnect($dbname = "uiradr", $username = "vada", $password = "zkouska", $servername = "localhost", $charset = "utf8") {
  try {
      $conn = new PDO("mysql:host=$servername;dbname=$dbname;charset=$charset", $username, $password);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      return $conn;
  }
  
  catch(PDOException $e) {
      echo "Connection failed: " . $e->getMessage();
      return null;
  }
}