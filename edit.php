<!DOCTYPE html>
<html lang='cs'>
    <head>
        <title>Editace učitele</title>
        <meta charset='utf-8'>
    </head>
    <body>
        <form action="teacher.php?update" method="POST">
            <label>Jméno</label>
            <input autofocus type="text" name="jmeno" value="<?php echo $ucitel->jmeno; ?>" required />
            <label>Příjmení</label>
            <input type="text" name="prijmeni" value="<?php echo $ucitel->prijmeni; ?>" required />
            <input type="hidden" name="uid" value="<?php echo $ucitel->id_ucitele; ?>" />
            <input type="submit" name="submit_edit_ucitele" value="Uložit" />
        </form>
    </body>
</html>